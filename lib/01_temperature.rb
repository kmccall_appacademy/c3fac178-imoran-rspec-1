def ftoc(fahrenheit)
  ((fahrenheit - 32) / 1.8).round
end

def ctof(celsius)
  ((celsius.to_f * 1.8) + 32).round(2)
end
