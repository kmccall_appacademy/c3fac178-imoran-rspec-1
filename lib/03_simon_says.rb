def echo(word)
  word
end

def shout(words)
  words.upcase
end

def repeat(word, num = 2)
  ("#{word} " * num).split(' ').join(' ')
end

def start_of_word(word, letter = 1)
  word[0...letter]
end

def first_word(word)
  word.split(' ')[0]
end

def titleize(sentence)
  sentence = sentence.split(' ')
  sentence.each_with_index do |word, index|
    word.capitalize! if !'the over and'.include?(word) || index.zero?
  end
  sentence.join(' ')
end
