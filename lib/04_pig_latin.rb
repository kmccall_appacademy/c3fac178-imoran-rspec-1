def translate(sentence)
  result = ''
  sentence.split(' ').each do |word|
    if 'aeiou'.include?(word[0])
      result += result == '' ? word + 'ay' : ' ' + word + 'ay'
    else
      result = shift_letters_until_vowel(word, result)
      result = check_result_for_caps(result)
    end
  end
  result
end

def check_result_for_caps(result)
  result = result.split(' ')
  result.each do |word|
    word.capitalize! if /[[:upper:]]/ =~ word
  end
  result.join(' ')
end

def shift_letters_until_vowel(word, result)
  word = word.split('')
  letter_not_vowel = word.find { |x| 'aeiou'.include?(x) }
  removed = check_for_qu(word, letter_not_vowel)
  space_bw_words(word, removed, result)
end

def check_for_qu(word, letter_not_vowel)
  removed =
    if /qu/ =~ word.join('')
      word.slice!(0..word.index(letter_not_vowel))
    else
      word.slice!(0...word.index(letter_not_vowel))
    end
  removed
end

def space_bw_words(word, removed, result)
  result +=
    if result == ''
      word.join('') + removed.join('') + 'ay'
    elsif result != ''
      ' ' + word.join('') + removed.join('') + 'ay'
    end
  result
end
