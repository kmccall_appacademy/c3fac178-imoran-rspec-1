def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(nums)
  nums != [] ? nums.reduce(:+) : 0
end

def multiply(nums)
  nums != [] ? nums.reduce(:*) : 0
end

def power(base, exponent)
  base**exponent
end

def factorial(num)
  (1..num).reduce(:*) || 1
end
